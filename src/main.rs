use std::io::prelude::*;
use std::time::Duration;
use std::f32;
use std::io::Cursor;
use std::collections::HashMap;

						
use std::ptr;
#[macro_use]




extern crate glium;
extern crate nalgebra as na;
extern crate image;

use glium::{DisplayBuild,Surface,Program,VertexBuffer,Frame};
use glium::index::IndexBuffer;
use glium::glutin::*;
use glium::uniforms::{Sampler, MagnifySamplerFilter,MinifySamplerFilter,SamplerWrapFunction};
use glium::backend::glutin_backend::GlutinFacade;
use glium::texture::{Texture2d,RawImage2d,DepthFormat};
use glium::framebuffer::{SimpleFrameBuffer,DepthRenderBuffer};
use na::{Vec3, Rot3, UnitQuat, Rotation, Mat4};
use std::fs::File;
use std::thread::sleep;
use std::f32::consts::PI;
use std::path::PathBuf;
use std::env;


#[derive(Copy, Clone)]
struct VPosition
{
	position:[f32;3]	
}
implement_vertex!(VPosition,position);

#[derive(Copy, Clone)]
struct VNormal
{
	normal: [f32;3]
}
implement_vertex!(VNormal,normal);

#[derive(Copy, Clone)]
struct VUV
{
	uv: [f32;2]
}
implement_vertex!(VUV,uv);

#[derive(Copy, Clone)]
struct VColor
{
	color: [f32;3]
}
implement_vertex!(VColor,color);



enum RenderTarget<'a>
{
	Screen(Frame),
	FrameBuffer(&'a str),
	None,
}

struct Context<'a>
{	
	data: &'a Data<'a>,
	target: RenderTarget<'a>,
	camera: Camera,
	
	shader: &'a str,
	screen: Entity<'a>,
	framebuffers: HashMap<&'a str, SimpleFrameBuffer<'a>>,
	
}

struct Data<'a>
{
	display: GlutinFacade,
	shaders: HashMap<&'a str,Program>,
	models: HashMap<&'a str,Model>,
	textures: HashMap<&'a str,Texture2d>,
	
	framebuffer_textures: HashMap<&'a str,Texture2d>,
	depth_renderbuffers: HashMap<&'a str,DepthRenderBuffer>,
	
	
	shader_path: PathBuf,
	model_path: PathBuf,
	texture_path: PathBuf,
	
	x_res:u32,
	y_res:u32,
	pmatrix: [[f32;4];4],
}






struct Model
{
	position_buffer:  VertexBuffer<VPosition>,
	index_buffer:	 IndexBuffer<u32>,
	normal_buffer: VertexBuffer<VNormal>,
	uv_buffer: VertexBuffer<VUV>,
	color_buffer: VertexBuffer<VColor>,
}
struct ModelBuilder
{
	position_buffer:  Option<VertexBuffer<VPosition>>,
	index_buffer:	 Option<IndexBuffer<u32>>,
	normal_buffer: Option<VertexBuffer<VNormal>>,
	uv_buffer: Option<VertexBuffer<VUV>>,
	color_buffer: Option<VertexBuffer<VColor>>,
}




impl ModelBuilder
{

	fn new()->ModelBuilder
	{
		ModelBuilder{
			position_buffer: None, 
			normal_buffer: None, 
			index_buffer: None,
			uv_buffer: None,
			color_buffer: None
			}
	}
	
	fn positions(mut self,position_buffer: VertexBuffer<VPosition>)->ModelBuilder
	{
		self.position_buffer=Some(position_buffer);
		self
	}
	
	fn normals(mut self,normal_buffer: VertexBuffer<VNormal>)->ModelBuilder
	{
		self.normal_buffer=Some(normal_buffer);
		self
	}
	
	fn uvs(mut self,uv_buffer: VertexBuffer<VUV>)->ModelBuilder
	{
		self.uv_buffer=Some(uv_buffer);
		self
	}
	
	fn colors(mut self,color_buffer: VertexBuffer<VColor>)->ModelBuilder
	{
		self.color_buffer=Some(color_buffer);
		self
	}
	
	fn indices(mut self,index_buffer: IndexBuffer<u32>)->ModelBuilder{
		self.index_buffer=Some(index_buffer);
		self
	}

	fn finalize(self) -> Model
	{
		Model{
			position_buffer:self.position_buffer.unwrap(),
			normal_buffer: self.normal_buffer.unwrap(),
			index_buffer: self.index_buffer.unwrap(),
			uv_buffer: self.uv_buffer.unwrap(),
			color_buffer: self.color_buffer.unwrap()
			}
	}
	
}

enum VertexAttribute{
	VertexPosition,
	VertexNormal,
	VertexTangent,
	VertexBitangent,
	VertexIndex,
	VertexColor,
	VertexUV
}

fn get_attribute(name: &str)->Option<VertexAttribute>
{
	match name
	{
		"VertexPosition" => Some(VertexAttribute::VertexPosition),
		"VertexNormal" =>Some(VertexAttribute::VertexNormal),
		"VertexTangent" => Some(VertexAttribute::VertexTangent),
		"VertexBitangent" => Some(VertexAttribute::VertexBitangent),
		"VertexIndex" =>Some(VertexAttribute::VertexIndex),
		"VertexColor" => Some(VertexAttribute::VertexColor),
		"VertexUV" => Some(VertexAttribute::VertexUV),
		_=>None,
	}
}


fn vec_to_3f(vec: Vec<&str>)->Option<[f32;3]>
{
	if vec.len()!=3
	{
		panic!(format!("{}: wrong coordinate size!",vec.len()))
	}
	let mut arr:[f32;3]=[0.,0.,0.];
	for (i,element) in vec.iter().enumerate()
	{
		match element.trim().parse()
		{
			Ok(x) => {arr[i]=x},
			Err(_) => {panic!("didn't parse!")},
		}
	}
	Some(arr)
}


fn vec_to_2f(vec: Vec<&str>)->Option<[f32;2]>
{
	if vec.len()!=2
	{
		panic!(format!("{}: wrong coordinate size!",vec.len()))
	}
	let mut arr:[f32;2]=[0.,0.];
	for (i,element) in vec.iter().enumerate()
	{
		match element.trim().parse()
		{
			Ok(x) => {arr[i]=x},
			Err(_) => {panic!("didn't parse!")},
		}
	}
	Some(arr)
}






impl<'a> Data<'a>{

	fn object(&self, model: &'a str)->Entity
	{
		Entity{
			position:[0.,0.,0.],
			scale:[1.,1.,1.],
			rotation:na::one(),
			tex0: Sampler::new(self.texture("ERR")).magnify_filter(MagnifySamplerFilter::Nearest),
			tex1: Sampler::new(self.texture("ERR")).magnify_filter(MagnifySamplerFilter::Nearest),
			tex2: Sampler::new(self.texture("ERR")).magnify_filter(MagnifySamplerFilter::Nearest),
			tex3: Sampler::new(self.texture("ERR")).magnify_filter(MagnifySamplerFilter::Nearest),
			model:self.model(model)}
	}

	fn new(w: u32,h:u32,fullscreen: bool)->Data<'a>
	{
	
		let monitor: glium::glutin::MonitorId=get_primary_monitor();	
		let mut x_res:u32= w;
		let mut y_res:u32= h;			
		let (x_res_screen, y_res_screen) = monitor.get_dimensions();	
		
		
		let display;
		
		if fullscreen
		{
			x_res=x_res_screen;
			y_res=y_res_screen;
			 display = WindowBuilder::new()
				.with_fullscreen(monitor)
				.with_depth_buffer(24)
				.with_decorations(false)
				.build_glium()
				.expect("failed to open the window!");
			
		}else{
			display = WindowBuilder::new()
				.with_dimensions(x_res, y_res)
				.with_pixel_format(24,8)
				.with_depth_buffer(24)
				.with_decorations(false)
				.build_glium()
				.expect("failed to open the window!");

		}		
			
			
			let pmatrix=perspective(x_res as f32/y_res as f32,90.,0.2,100.);
			
		
		
		
		{
			let ref window = &display.get_window().expect("couldn't obtain a ref to the window");
			window.set_position(((x_res_screen-x_res)/2) as i32, ((y_res_screen -y_res)/2) as i32);
			
			window.set_cursor_state(CursorState::Grab).expect("failed to set cursor state");
			window.set_cursor_position(256,256).expect("failed to set cursor position");
			window.set_cursor(MouseCursor::NoneCursor);
		}
		
		let mut target= display.draw();
		target.clear_color(0.,0.,0.,1.);
		target.finish().unwrap();
		let shaders=HashMap::new();
		let textures=HashMap::new();
		let models=HashMap::new();	
		
		let framebuffer_textures=HashMap::new();
		let depth_renderbuffers=HashMap::new();	
		
		

		
		
		
		Data{
			shaders: shaders,
			models: models,
			textures: textures,
			x_res:x_res,
			y_res:y_res,
			pmatrix:pmatrix,
			framebuffer_textures:framebuffer_textures,
			depth_renderbuffers:depth_renderbuffers,
			shader_path: env::current_dir().expect("couldn't get the currend directory"),
			model_path: env::current_dir().expect("couldn't get the currend directory"),
			texture_path: env::current_dir().expect("couldn't get the currend directory"),
			display: display
			}.load_texture("ERR","ERR.png").load_model("plane","plane")
	
	}
	
	fn create_framebuffer(mut self, name: &'a str)->Data<'a>
	{
		let ftex=Texture2d::empty(&self.display,self.x_res,self.y_res).unwrap();
		let fdepth=DepthRenderBuffer::new(&self.display,	DepthFormat::I24,self.x_res,self.y_res).unwrap();
		self.framebuffer_textures.insert(name,ftex);
		self.depth_renderbuffers.insert(name,fdepth);
		self
	}

	
	fn load_shader(mut self, name:&'a str, vertex: &str, fragment: &str)->Data<'a>
	{
		let mut vpath=self.shader_path.clone();
		vpath.push(vertex);
		vpath.set_extension("vertex");
		let mut fpath=self.shader_path.clone();
		fpath.push(fragment);
		fpath.set_extension("fragment");
		
		let mut vertex_shader_src = String::new();
		let mut fragment_shader_src = String::new();
		
		let mut f=File::open(vpath).expect(&format!("{}: failed to open a vertex shader file!",&vertex));
		f.read_to_string(&mut vertex_shader_src).expect(&format!("{}: failed to read vertex shader!",&vertex));
		
		let mut f=File::open(fpath).expect(&format!("{}: failed to open a fragment shader file!",&fragment));
		f.read_to_string(&mut fragment_shader_src).expect(&format!("{}: failed to read fragment shader!",&fragment));
		
		let shader = Program::from_source(&self.display, &vertex_shader_src, &fragment_shader_src, None).expect(&format!("{}, {}: failed to compile the shader program!",&vertex,&fragment));
		self.shaders.insert(name,shader);
		self
	}
	
	fn load_texture(mut self,  name: &'a str, filename: &str)->Data<'a>
	{
		let mut path=self.texture_path.clone();
		path.push(filename);
		let image = image::open(path).expect(&format!("{}: failed to open the texture file!",&filename)).to_rgba();
			
		let image_dimensions = image.dimensions();
		let image = RawImage2d::from_raw_rgba_reversed(image.into_raw(), image_dimensions);
		let texture = Texture2d::new(&self.display, image).expect(&format!("{}: failed to create the texture!",&filename));
		self.textures.insert(name,texture);
		self
	}
	
	fn load_model(mut self, name:&'a str, filename: &str) -> Data<'a>
	{
		let mut path=self.model_path.clone();
		path.push(filename);
		path.set_extension("model");
		
		let mut f=File::open(path).expect(&format!("{}: failed to open a model file!",&filename));
		let mut model_src=String::new();
		f.read_to_string(&mut model_src).expect(&format!("{}: failed to read the model file!",&filename));
		
		let attrs=model_src.trim().split("#");
		

		let mut positions=Vec::new();
		let mut normals=Vec::new();
		let mut indices=Vec::new();
		let mut colors=Vec::new();
		let mut uvs=Vec::new();
		
		
		for attr in attrs
		{
			if attr.len()==0{continue}
			
			let mut lines:Vec<&str>=attr.trim().split("\n").collect();
			
			let name=lines.remove(0).trim();
			let attribute:VertexAttribute=get_attribute(name).expect(&format!("{} - {}: unknown attribute!",&filename,&name));

			for line in lines
			{
				let coords: Vec<&str>= line.trim().split(" ").collect();
				match attribute{
					VertexAttribute::VertexPosition =>
					{	positions.push(VPosition{position:vec_to_3f(coords).expect(&format!("{}:failed to parse coordinates!",&filename))});	
					},
					VertexAttribute::VertexNormal =>
					{	normals.push(VNormal{normal:vec_to_3f(coords).expect(&format!("{}:failed to parse normals!",&filename))});	
					},
					VertexAttribute::VertexUV =>
					{	uvs.push(VUV{uv:vec_to_2f(coords).expect(&format!("{}:failed to parse uvs!",&filename))});	
					},
					VertexAttribute::VertexColor =>
					{	colors.push(VColor{color:vec_to_3f(coords).expect(&format!("{}:failed to parse colors!",&filename))});	
					},
					VertexAttribute::VertexIndex =>
					{
						assert!(coords.len()==3, format!("{}: wrong number of indices in some of the entries!",&filename));
						for coord in coords
						{
							indices.push(coord.parse().expect(&format!("{}: couldn't parse some of the indices!",&filename)));
						}
						
					},
					_=>(),
				}
			}
		}
		let position_buffer = VertexBuffer::new(&self.display, &positions)
			.expect(&format!("{}: failed to create a vertex buffer!",&filename));
		let normal_buffer = VertexBuffer::new(&self.display, &normals)
			.expect(&format!("{}: failed to create a normal buffer!",&filename));
		let index_buffer = IndexBuffer::new(&self.display, glium::index::PrimitiveType::TrianglesList, &indices)
			.expect(&format!("{}: failed to create an index buffer!",&filename));
		let uv_buffer = VertexBuffer::new(&self.display, &uvs)
			.expect(&format!("{}: failed to create a uv buffer!",&filename));
		let color_buffer = VertexBuffer::new(&self.display, &colors)
			.expect(&format!("{}: failed to create a color buffer!",&filename));
		let model = ModelBuilder::new()
			.positions(position_buffer)
			.indices(index_buffer)
			.normals(normal_buffer)
			.uvs(uv_buffer)
			.colors(color_buffer)
			.finalize();
		self.models.insert(name,model);
		self
	}
	
	
	fn finalize(self)->Data<'a>
	{
		self

	}
	
	fn shader(&self, name:&'a str)->&Program
	{
		self.shaders.get(&name).as_ref().expect(&format!("{}: no such shader",&name))
	
	}
	fn model(&self, name:&'a str)->&Model
	{
		self.models.get(&name).as_ref().expect(&format!("{}: no such model",&name))
	
	}
	fn texture(&self, name:&'a str)->&Texture2d
	{
		self.textures.get(&name).as_ref().expect(&format!("{}: no such texture",&name))
	}
	fn ftex(&self, name:&'a str)->&Texture2d
	{
		self.framebuffer_textures.get(&name).as_ref().expect(&format!("{}: no such framebuffer texture",&name))
	}
	fn fdepth(&self, name:&'a str)->&DepthRenderBuffer
	{
		self.depth_renderbuffers.get(&name).as_ref().expect(&format!("{}: no such depth renderbuffer",&name))
	}

	
	
		
}


fn perspective(ratio:f32, angle:f32, n:f32, f:f32)->[[f32;4];4]
{
	let r=0.1;
	let t=0.1;

	let tan:f32=(angle.to_radians()/2.).tan();

	[[n/(r*ratio*tan), 0., 0., 0.],
     [0., n/(t*tan), 0., 0.],
     [0., 0., -(f+n)/(f-n), -1.],
     [0., 0., -2.*f*n/(f-n), 0.]]
}


impl<'a> Context<'a>
{
	fn new(data: &'a Data)->Context<'a>
	{		
		let mut framebuffers=HashMap::new();
		for name in data.framebuffer_textures.keys()
		{
			let framebuffer = SimpleFrameBuffer::with_depth_buffer(&data.display, data.ftex(name), data.fdepth(name)).expect("nope");
			framebuffers.insert(*name,framebuffer);
			
		}
		
		
		
		Context{
			data: data,
			framebuffers:framebuffers,
			target:RenderTarget::None,
			camera:Camera::new(),
			screen: data.object("plane"),
			shader: "",
			}
	
	}

	
	
	fn draw(&mut self, obj: &Entity)
	{
		let v = (
			&(obj.model.position_buffer),
			&(obj.model.normal_buffer),
			&(obj.model.uv_buffer),
			&(obj.model.color_buffer),
			);
			
		
		
		let uniforms = uniform!{
			rot:*obj.rotation.quat().as_ref(),
			pos:obj.position,
			scale:obj.scale,
			pMatrix:self.data.pmatrix,
			tex0:obj.tex0,
			tex1:obj.tex1,
			tex2:obj.tex2,
			tex3:obj.tex3,
			cameraPosition: self.camera.position, 
			cameraRotation: *self.camera.rotation.quat().as_ref(),
			balls:8
			};
		
		let parameters=glium::DrawParameters{
			depth:glium::Depth{
				test:glium::draw_parameters::DepthTest::IfLess,
				write: true,
				..Default::default()
				},
				..Default::default()
			};
		
		match self.target{
			RenderTarget::Screen(ref mut frame)=>
				frame.draw(
					v, 
					&(obj.model.index_buffer), 
					self.data.shader(self.shader), 
					&uniforms,
					&parameters).expect("failed drawing a thing"),
			RenderTarget::FrameBuffer(buffer)=>
				self.framebuffers.get_mut(buffer).as_mut().unwrap().draw(
					v, 
					&(obj.model.index_buffer), 
					self.data.shader(self.shader), 
					&uniforms,
					&parameters).expect("failed drawing a thing"),
			
			RenderTarget::None=>panic!("no rendering target"),
			}
			
	}
	
	fn use_shader(&mut self, name: &'a str)
	{
		self.shader=name;
	}
	
	fn use_framebuffer(&mut self,  name: &'a str)
	{
		self.target=RenderTarget::FrameBuffer(name);
	}
	fn use_screen(&mut self)
	{
		self.target=RenderTarget::Screen(self.data.display.draw());
	}
	
	fn finish_drawing(mut self)->Context<'a>
	{
		match self.target
			{
				RenderTarget::Screen(frame)=>frame.finish().unwrap(),
				_=>panic!("trying to finish drawing with a wrong target"),
			};
		self.target=RenderTarget::None;
		self
	}

	
	
	fn clear(&mut self)
	{
		match self.target
		{
			RenderTarget::Screen(ref mut frame)=>
				frame.clear_color_and_depth((0.,0.,0.,1.0),1.),
			RenderTarget::FrameBuffer(buffer)=>
				self.framebuffers.get_mut(buffer).as_mut().unwrap().clear_color_and_depth((0.,0.,0.,1.0),1.),
			RenderTarget::None=>panic!("no rendering target"),
		}
	}
	
	fn clear_color(&mut self,r:f32,g:f32,b:f32,a:f32)
	{
	
		match self.target
		{
			RenderTarget::Screen(ref mut frame)=>
				frame.clear_color_and_depth((r,g,b,a),1.),
			RenderTarget::FrameBuffer(buffer)=>
				self.framebuffers.get_mut(buffer).as_mut().unwrap().clear_color_and_depth((r,g,b,a),1.),
			RenderTarget::None=>panic!("no rendering target"),
		}
	}
	

}






struct Entity<'a>
{	
	model: &'a Model,
	tex0: Sampler<'a,Texture2d>,
	tex1: Sampler<'a,Texture2d>,
	tex2: Sampler<'a,Texture2d>,
	tex3: Sampler<'a,Texture2d>,
	rotation: na::UnitQuat<f32>,
	position:[f32;3],
	scale:[f32;3],
}

impl<'a> Entity<'a>
{
	fn texture(&mut self, index:i32,tex:&'a Texture2d)
	{
		match index{
		0=>self.tex0=Sampler::new(tex),
		1=>self.tex1=Sampler::new(tex),
		2=>self.tex2=Sampler::new(tex),
		3=>self.tex3=Sampler::new(tex),
		_=>(),
		}
	}
	
	fn magnify_filter(&mut self, index:i32, filter:MagnifySamplerFilter)
	{
		match index{
		0=>self.tex0=self.tex0.magnify_filter(filter),
		1=>self.tex1=self.tex1.magnify_filter(filter),
		2=>self.tex2=self.tex2.magnify_filter(filter),
		3=>self.tex3=self.tex3.magnify_filter(filter),
		_=>(),
		}
		
	}
	fn minify_filter(&mut self, index:i32, filter:MinifySamplerFilter)
	{
		match index{
		0=>self.tex0=self.tex0.minify_filter(filter),
		1=>self.tex1=self.tex1.minify_filter(filter),
		2=>self.tex2=self.tex2.minify_filter(filter),
		3=>self.tex3=self.tex3.minify_filter(filter),
		_=>(),
		}
		
	}
	fn wrap_function(&mut self, index:i32, func:SamplerWrapFunction)
	{
		match index{
		0=>self.tex0=self.tex0.wrap_function(func),
		1=>self.tex1=self.tex1.wrap_function(func),
		2=>self.tex2=self.tex2.wrap_function(func),
		3=>self.tex3=self.tex3.wrap_function(func),
		_=>(),
		}
	
	}
	fn anisotropy(&mut self, index:i32, level: u16)
	{
		match index{
		0=>self.tex0=self.tex0.anisotropy(level),
		1=>self.tex1=self.tex1.anisotropy(level),
		2=>self.tex2=self.tex2.anisotropy(level),
		3=>self.tex3=self.tex3.anisotropy(level),
		_=>(),
		}
		
	}
	
	
	
	fn set_scale(&mut self, x:f32,y:f32,z:f32)
	{
		self.scale=[x,y,z];
	}
	
	fn mmove(&mut self, x:f32,y:f32,z:f32)
	{
		self.position[0]+=x;
		self.position[1]+=y;
		self.position[2]+=z;
	}
	fn place(&mut self,  x:f32,y:f32,z:f32)
	{
		self.position=[x,y,z];
	}
	
	
	fn rotate(&mut self,  x:f32,y:f32,z:f32)
	{
		self.rotation=self.rotation*UnitQuat::new_with_euler_angles(x.to_radians(),y.to_radians(),z.to_radians());
	}
	
	fn set_rotation(&mut self,  x:f32,y:f32,z:f32)
	{
		self.rotation=UnitQuat::new_with_euler_angles(x.to_radians(),y.to_radians(),z.to_radians());
	}

}


struct Camera
{
	position: [f32;3],
	rotation: na::UnitQuat<f32>,
}




impl Camera{
	fn new()->Camera
	{
		Camera{position:[0.,0.,5.],rotation: na::one()}	
	}
	
	fn mmove(&mut self, x:f32,y:f32,z:f32)
	{
		self.position[0]+=x;
		self.position[1]+=y;
		self.position[2]+=z;
	}
	fn place(&mut self,  x:f32,y:f32,z:f32)
	{
		self.position=[x,y,z];
	}
	
	
	fn rotate(&mut self,  x:f32,y:f32,z:f32)
	{
		self.rotation=self.rotation*UnitQuat::new_with_euler_angles(x.to_radians(),y.to_radians(),z.to_radians());
	}
	
	fn set_rotation(&mut self,  x:f32,y:f32,z:f32)
	{
		self.rotation=UnitQuat::new_with_euler_angles(x.to_radians(),y.to_radians(),z.to_radians());
	}	
}

struct Controls
{
	fwd:f32,
	bck:f32,
	lft:f32,
	rgt:f32,
	mx:f32,
	my:f32,
	vx:f32,
	vy:f32,
	vz:f32,
	dx:f32,
	dy:f32,
	pitch:f32,
	yaw:f32,
}

impl Controls
{
	fn new()->Controls
	{
		Controls
		{
			fwd:0.,
			bck:0.,
			lft:0.,
			rgt:0.,
			mx:256.,
			my:256.,
			vx:0.,
			vy:0.,
			vz:0.,
			dx:0.,
			dy:0.,
			pitch:0.,
			yaw:0.,
		}
	}

}



fn main() {	
	let mut data=Data::new(800,800,false)
		.load_texture("test","test.png")
		.load_shader("shader","test","test")
		.load_shader("overlay","screen","overlay")
		.load_shader("glitch","screen","glitch")
		.load_shader("deform","screen","deform")
		.create_framebuffer("fb")
		.finalize();
	

	
	let mut ctx=Context::new(&data);
	
	let mut obj=data.object("plane");	
	let mut obj2=data.object("plane");
	let mut screen=data.object("plane");
	
	obj.texture(0,data.texture("test"));
	obj2.texture(0,data.texture("test"));
	screen.texture(0,data.ftex("fb"));
	
	obj2.place(0.,-2.,0.);
	obj2.set_scale(10.,10.,1.);
	obj2.set_rotation(90.,0.,0.);
	
	let mut shader2="overlay";
	
	
		
	
	
	
	let mut controls = Controls::new();
	let mut pause=false;
	let mut fullscreen=false;
	loop
	{
		
		let dx = controls.lft-controls.rgt;
		let dz = controls.fwd-controls.bck;
		
		let angle =dx.atan2(dz)+ PI+controls.yaw.to_radians();
		
		if controls.vy>-1.
		{
			controls.vy-=0.01;
		}
		else
		{
			controls.vy=-1.;
		}
		
		
		if ctx.camera.position[1]+controls.vy<0.
		{
			ctx.camera.position[1]=0.;
		}else{
			ctx.camera.position[1]+=controls.vy;
		
		}
		
		
		if dz!=0. || dx!=0.
		{		
			ctx.camera.mmove(
				angle.sin()*0.1,
				0.,
				angle.cos()*0.1);
		}
		
		if !pause
		{
			ctx.target=RenderTarget::FrameBuffer("fb");
			ctx.use_shader("shader");
			ctx.clear();
			ctx.draw(&obj);	
			ctx.draw(&obj2);
			ctx.use_screen();
			ctx.use_shader(shader2);
			ctx.clear();
			ctx.draw(&screen);
			ctx=ctx.finish_drawing();
			
			
		}else{
			ctx.use_screen();
			ctx.clear_color(1.,0.,0.,1.);
			ctx=ctx.finish_drawing();
		
		}
		
		
	
		
			let ref window = &data.display.get_window().expect("couldn't obtain a ref to the window");
			for ev in data.display.poll_events()
			{
			
				match ev
				{
					Event::Closed => return,
					Event::KeyboardInput(state, code, Some(vcode)) => {
					use glium::glutin::VirtualKeyCode::*;		
							println!("{} = {:?}", code, vcode);
							match state{
							ElementState::Pressed=>
								match vcode 
								{
									
									Q | Escape =>  return,
									W | Up => controls.fwd=1.,
									A | Left => controls.lft=1.,
									S | Down => controls.bck=1.,
									D | Right => controls.rgt=1.,
									Space=>
									{
										if ctx.camera.position[1]==0.
										{
											controls.vy=0.2
										}
									},
									Tab=>shader2=match shader2
									{
										"overlay"=>"glitch",
										"glitch"=>"deform",
										_=>"overlay",
									
									},
									Return=>
									{
										if fullscreen
											{
												
											
												
											}
											else
											{
												
																					
											
												
											}
									
									
									},
									_=> (),
								},
								
							ElementState::Released=>
								match vcode 
								{
									W | Up => controls.fwd=0.,
									A | Left => controls.lft=0.,
									S | Down => controls.bck=0.,
									D | Right => controls.rgt=0.,
									
										
									_ => (),
								},
						};
					},
					Event::MouseMoved((x,y))=>
					{
						if !pause
						{
							
							controls.dx=256.-(x as f32);
							controls.dy=256.-(y as f32);
							controls.pitch+=controls.dy*0.1;
							controls.yaw+=controls.dx*0.1;
							ctx.camera.set_rotation(controls.pitch,controls.yaw,0.);
							window.set_cursor_position(256,256).expect("failed to set cursor position");
						}
					},
					Event::Focused(ye)=>
					{
						match ye
						{
							true=>
							{
								window.set_cursor(MouseCursor::NoneCursor);
								window.set_cursor_state(CursorState::Grab).expect("failed to set cursor state");
								pause=false;
							},
							false=>
							{
								window.set_cursor(MouseCursor::Default);
								window.set_cursor_state(CursorState::Normal).expect("failed to set cursor state");
								pause=true;	
							},
						}
					}
					_=>()
				}
			}
		sleep(Duration::from_millis(16));
	}
}